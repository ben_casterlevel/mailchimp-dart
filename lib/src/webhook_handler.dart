class WebhookHandler {
  Function(Map<String, dynamic> data) callback;
  String type;

  WebhookHandler(this.type, this.callback);
}

class MailchimpWebhookHandler {
  final Map<String, void Function(Map<String, dynamic> data)> handlers = {};

  static const relevantFields = <String, String>{
    'data[email]': 'email',
    'fired_at': 'updated',
    'data[list_id]': 'listId',
    'data[old_email]': 'email',
    'data[new_email]': 'newEmail',
  };

  void registerHandler(WebhookHandler handler) =>
      handlers[handler.type] = handler.callback;

  void handleWebhook(Map<String, dynamic> map) {
    assert(map.containsKey('type'));
    print('[Mailchimp] Parsing webhook: $map');
    final key = map['type'];
    final type = key is List ? key.first : key;
    if (!handlers.containsKey(type)) {
      throw Exception('Unsupported webhook type $type');
    }

    final data = <String, dynamic>{};
    for (final mailchimpKey in relevantFields.keys) {
      if (map.containsKey(mailchimpKey)) {
        final value = map[mailchimpKey];
        data[relevantFields[mailchimpKey]] =
            value is List ? value.first : value;
      }
    }
    print('[Mailchimp] Passing data to handler $type: $data');
    handlers[type](data);
  }
}
