import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'package:crypto/crypto.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

import 'client.dart';

class MailingList {
  static const onListStatuses = <MailchimpSubscriberStatus>[
    MailchimpSubscriberStatus.cleaned,
    MailchimpSubscriberStatus.subscribed,
    MailchimpSubscriberStatus.unsubscribed,
    MailchimpSubscriberStatus.pending,
  ];
  final MailchimpClient _mailchimp;

  final Logger log = Logger('mailchimp');
  final String listId;

  MailingList({
    @required MailchimpClient client,
    @required this.listId,
  })  : assert(client != null && listId != null),
        _mailchimp = client;

  Future<MailchimpSubscriberStatus> subscriptionStatus(email) async {
    // To get status of subscriber: GET /3.0/lists/$listId/members/$md5
    //
    // To get the MD5 hash of the email address Urist.McVankab@freddiesjokes.com,
    // first convert the address to its lowercase version. The MD5 hash of
    // urist.mcvankab@freddiesjokes.com is 62eeb292278cc15f5817cb78f7790b08.
    //
    // If the call returns a 404 response, the subscriber isn’t on your list. They
    // may have been deleted, or they were never on the list at all.
    //
    // If the call returns a 200 response, check the status field. You’ll see one of
    // these labels in the status field.
    final hash =
        md5.convert(utf8.encode(email.toString().toLowerCase())).toString();
    final request = await _mailchimp.getUrl('/3.0/lists/$listId/members/$hash');
    log.info('Checking subscription status for $email');
    final response = await request.send();

    final String responseString =
        await response.stream.transform(utf8.decoder).join();
    final Map<String, dynamic> responseBody =
        json.decode(responseString.toString()) as Map<String, dynamic>;
    final status = await _getStatusFromResponseBody(response, responseBody);
    log.info('Received subscription status for $email: $status');
    return status;
  }

  Future<MailchimpSubscriberStatus> subscribe({
    @required String name,
    @required String email,
    String signupSource,
  }) async {
    // To subscribe a member: POST /3.0/lists/$listId/members/
    //
    // Example body:
    //     {
    //         'email_address': 'urist.mcvankab@freddiesjokes.com',
    //         'status': 'subscribed',
    //         'merge_fields': {
    //           'FNAME': 'Urist',
    //           'LNAME': 'McVankab'
    //         }
    //     }
    //
    // Use pending to send a confirmation email.
    log.info('Building subscription request for $email');
    final body = <String, dynamic>{
      'email_address': email,
      'status': 'pending',
      'merge_fields': <String, dynamic>{
        'SOURCE': signupSource ?? _mailchimp.clientName,
      },
    };

    if (name != null) {
      if (name.contains(' ')) {
        final names = name.split(' ');
        body['merge_fields']['FNAME'] =
            names.sublist(0, names.length - 1).join(' ');
        body['merge_fields']['LNAME'] = names.last;
      } else {
        body['merge_fields']['FNAME'] = name;
      }
    }

    final jsonBytes = utf8.encode(json.encode(body));
    final request = await _mailchimp.postUrl('/3.0/lists/$listId/members');
    log.info('Sending subscription request: ${json.encode(body)}');
    request.bodyBytes = jsonBytes;

    final response = await request.send();
    final String responseString =
        await response.stream.transform(utf8.decoder).join();

    Map<String, dynamic> responseBody;
    if (responseString != null && responseString.isNotEmpty) {
      responseBody =
          json.decode(responseString.toString()) as Map<String, dynamic>;
    }

    if ((response.contentLength ?? 0) > 0) {
      log.info('Received response for $email: $responseBody');
    } else {
      log.info('Received empty for $email');
    }

    final status = await _getStatusFromResponseBody(response, responseBody);
    log.info('Calculated status for $email: $status');
    if (status == MailchimpSubscriberStatus.exists) {
      return subscriptionStatus(email);
    }

    return status;
  }

  String _getErrorFromResponse(
      http.StreamedResponse response, Map<String, dynamic> responseBody) {
    String error = 'Unexpected status code (${response.statusCode})';
    if ((response.contentLength ?? 0) > 0) {
      error += ": \n";
      error += responseBody.toString();
    }
    return error;
  }

  MailchimpSubscriberStatus _getStatusFromResponseBody(
    http.StreamedResponse response,
    Map<String, dynamic> responseBody,
  ) {
    switch (response.statusCode) {
      case HttpStatus.ok:
        break;
      case HttpStatus.notFound:
        return MailchimpSubscriberStatus.not_subscribed;
      case HttpStatus.found:
        return MailchimpSubscriberStatus.exists;
      case HttpStatus.badRequest:
        if ((response.contentLength ?? 0) > 0) {
          if (responseBody['title'] == 'Member Exists') {
            return MailchimpSubscriberStatus.exists;
          }
        }
        throw _getErrorFromResponse(response, responseBody);
      default:
        throw _getErrorFromResponse(response, responseBody);
    }

    switch (responseBody['status'] as String) {
      case ('pending'):
        return MailchimpSubscriberStatus.pending;
      case ('subscribed'):
        return MailchimpSubscriberStatus.subscribed;
      case ('cleaned'):
        return MailchimpSubscriberStatus.cleaned;
      case ('unsubscribed'):
        return MailchimpSubscriberStatus.unsubscribed;
      default:
        return MailchimpSubscriberStatus.error;
    }
  }
}
