import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import 'mailing_list.dart';

class MailchimpClient {
  final String _host;
  final String _authHeader;
  final String clientName;
  final client = http.Client();

  MailchimpClient({
    @required String host,
    @required String this.clientName,
    @required String clientSecret,
  })  : assert(host != null && clientName != null && clientSecret != null),
        _host = host,
        _authHeader =
            'Basic ${base64.encode(utf8.encode('$clientName:$clientSecret'))}';

  // For documentation, see:
  // http://developer.mailchimp.com/documentation/mailchimp/guides/get-started-with-mailchimp-api-3/

  Future<http.Request> getUrl(String path) async => _buildRequest('GET', path);

  MailingList mailingList(String listId) =>
      MailingList(client: this, listId: listId);

  Future<http.Request> postUrl(String path) async =>
      _buildRequest('POST', path);

  Future<http.Request> _buildRequest(String verb, String path) async {
    final uri = Uri(host: _host, scheme: 'https', path: path);
    final request = http.Request(verb, uri)
      ..headers[HttpHeaders.authorizationHeader] = _authHeader;
    print('${request.method} ${request.url}');
    return request;
  }

  static bool eligibleToSubscribe(MailchimpSubscriberStatus status) =>
      status == MailchimpSubscriberStatus.not_subscribed;
}

enum MailchimpSubscriberStatus {
  pending,
  subscribed,
  unsubscribed,
  cleaned,
  not_subscribed,
  processing,
  error,
  exists,
}

String mailchimpSubscriberStatusToString(MailchimpSubscriberStatus status) =>
    status.toString().split('.').last;
