import 'package:mailchimp/mailchimp.dart';
import 'package:test/test.dart';

main() async {
  final MailchimpClient client = MailchimpClient(
    host: "us17.api.mailchimp.com",
    clientName: "com.spelltrackerapp.pathfinder",
    clientSecret: "a1c5b3155dbe6d796e40671c3ce66387-us17",
  );

  final MailingList list = client.mailingList("727557d531");

  group("subscriptionStatus", () {
    test("when it should be subbed", () async {
      final subbedStatus = await list.subscriptionStatus("ben@casterlevel.com");
      expect(subbedStatus, equals(MailchimpSubscriberStatus.subscribed));
    });
    test("when it should not be subbed", () async {
      final unsubbedStatus =
          await list.subscriptionStatus("notreal@benjaminasmith.com");
      expect(unsubbedStatus, equals(MailchimpSubscriberStatus.not_subscribed));
    });
  });

  group("subscribe", () {
    test(
      "when it is an existing address and should already be subbed",
      () async {
        final subbedStatus =
            await list.subscribe(name: "Ben", email: "ben@casterlevel.com");
        expect(subbedStatus, equals(MailchimpSubscriberStatus.subscribed));
      },
      skip: "Uses real API, best not to clutter the list.",
    );

    test(
      "when it is a new address and should be pending after the request",
      () async {
        final datetime = DateTime.now().millisecondsSinceEpoch.toString();
        final email = "ben.$datetime@casterlevel.com";
        final subbedStatus = await list.subscribe(name: "Name", email: email);
        expect(subbedStatus, equals(MailchimpSubscriberStatus.pending));
      },
      skip: "Uses real API, best not to clutter the list.",
    );
  });
}
