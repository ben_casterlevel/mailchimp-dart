import 'dart:convert';

import 'package:mailchimp/mailchimp.dart';
import 'package:test/test.dart';

final handler = MailchimpWebhookHandler();

void _testHandler(String body, String type, Map<String, dynamic> expected) {
  final result = <String, dynamic>{};
  final mock = WebhookHandler(
    type,
    (Map<String, dynamic> data) => result.addAll(data),
  );
  handler.registerHandler(mock);
  handler.handleWebhook(json.decode('{$body}') as Map<String, dynamic>);
  for (final key in expected.keys) {
    expect(result, containsPair(key, expected[key]));
  }
}

main() async {
  test("Subscribes", () async {
    final String body = """
"type": "subscribe",
"fired_at": "2009-03-26 21:35:57",
"data[id]": "8a25ff1d98",
"data[list_id]": "a6b5da1054",
"data[email]": "api@mailchimp.com",
"data[email_type]": "html",
"data[merges][EMAIL]": "api@mailchimp.com",
"data[merges][FNAME]": "Mailchimp",
"data[merges][LNAME]": "API",
"data[merges][INTERESTS]": "Group1,Group2",
"data[ip_opt]": "10.20.10.30",
"data[ip_signup]": "10.20.10.30"
""";
    _testHandler(body, 'subscribe', {
      'email': 'api@mailchimp.com',
      'listId': 'a6b5da1054',
    });
  });

  test("Unsubscribes", () async {
    final String body = """
"type": "unsubscribe",
"fired_at": "2009-03-26 21:40:57",
"data[action]": "unsub",
"data[reason]": "manual",
"data[id]": "8a25ff1d98",
"data[list_id]": "a6b5da1054",
"data[email]": "api+unsub@mailchimp.com",
"data[email_type]": "html",
"data[merges][EMAIL]": "api+unsub@mailchimp.com",
"data[merges][FNAME]": "Mailchimp",
"data[merges][LNAME]": "API",
"data[merges][INTERESTS]": "Group1,Group2",
"data[ip_opt]": "10.20.10.30",
"data[campaign_id]": "cb398d21d2",
"data[reason]": "hard"
""";
    _testHandler(body, 'unsubscribe', {
      'email': 'api+unsub@mailchimp.com',
      'listId': 'a6b5da1054',
    });
  });

  test("Profile Updates", () async {
    final String body = """
"type": "profile",
"fired_at": "2009-03-26 21:31:21",
"data[id]": "8a25ff1d98",
"data[list_id]": "a6b5da1054",
"data[email]": "api@mailchimp.com",
"data[email_type]": "html",
"data[merges][EMAIL]": "api@mailchimp.com",
"data[merges][FNAME]": "Mailchimp",
"data[merges][LNAME]": "API",
"data[merges][INTERESTS]": "Group1,Group2",
"data[ip_opt]": "10.20.10.30"
""";
    _testHandler(body, 'profile', {
      'email': 'api@mailchimp.com',
      'listId': 'a6b5da1054',
    });
  });

  test("Email Address Changes", () async {
    final String body = """
"type": "upemail",
"fired_at": "2009-03-26 22:15:09",
"data[list_id]": "a6b5da1054",
"data[new_id]": "51da8c3259",
"data[new_email]": "api+new@mailchimp.com",
"data[old_email]": "api+old@mailchimp.com"
""";
    _testHandler(body, 'upemail', {
      'email': 'api+old@mailchimp.com',
      'newEmail': 'api+new@mailchimp.com',
    });
  });

  test("Cleaned Emails", () async {
    final String body = """
"type": "cleaned",
"fired_at": "2009-03-26 22:01:00",
"data[list_id]": "a6b5da1054",
"data[campaign_id]": "4fjk2ma9xd",
"data[reason]": "hard",
"data[email]": "api+cleaned@mailchimp.com"
""";
    _testHandler(body, 'cleaned', {
      'email': 'api+cleaned@mailchimp.com',
      'listId': 'a6b5da1054',
    });
  });

  test("Campaign Sending Status", () async {
    final String body = """
"type": "campaign",
"fired_at": "2009-03-26 21:31:21",
"data[id]": "5aa2102003",
"data[subject]": "Test Campaign Subject",
"data[status]": "sent",
"data[reason]": "",
"data[list_id]": "a6b5da1054"
""";
    _testHandler(body, 'campaign', {
      'listId': 'a6b5da1054',
    });
  });
}
